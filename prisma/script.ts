import { PrismaClient } from '@prisma/client'
import * as bcrypt from 'bcrypt'

const prisma = new PrismaClient()
const passBycrpt = '12345678';

export async function main() {
  const hassPass = bcrypt.hash(passBycrpt, 10)
  const user = await prisma.user.create({
    data: {
      name: 'asep',
      email: 'assesment@email.com',
      password: String(hassPass),
    },
  })
  console.log(user)
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })