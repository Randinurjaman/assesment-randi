FROM node:20-alpine3.18 as base
 
WORKDIR /app
COPY package*.json yarn.lock* ./
COPY prisma ./prisma/
 
RUN npm install
 
 
COPY . .
 
RUN npx prisma generate
RUN apk add postgresql
 
COPY entrypoint.sh /entrypoint.sh
 
ENV PORT=3000
 
EXPOSE 3000
 
CMD ["yarn", "dev"]