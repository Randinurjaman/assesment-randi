# Wait for the database to be ready
until pg_isready -h data_base_assesment -p 5432 -U postgres; do
  echo "Waiting for database..."
  sleep 2
done
 
npx prisma migrate dev --name init
 
yarn dev
 
# Start the application
exec "$@"