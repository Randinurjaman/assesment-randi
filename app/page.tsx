'use client'
import { signIn, useSession } from "next-auth/react";
import { redirect } from "next/navigation";
import { FormEvent } from "react";
import React, { useState } from 'react';

export default function Home() {
  const [activeTab, setActiveTab] = useState<'user' | 'guest'>('user');
  
  const session = useSession()  
  if (session.data){
    return redirect('/task')
  }
  const handleLogin = async (data:FormEvent<HTMLFormElement>) => {
    data.preventDefault()
    const formData = new FormData(data.currentTarget)
    const connectAuth = await signIn('credentials',{
      email: activeTab === 'guest' ? 'guest@email.com' :  formData.get('email'), 
      password: activeTab === 'guest' ? '12345678' : formData.get('password'),
      redirect: false
    })
    if (connectAuth?.status !== 200) {
      console.log('email / password invalid')
    }
    console.log(connectAuth?.status === 200)
    console.log(formData.get('password'))
  }  
  console.log(session)
  return (
    <>
      <div className="bg-gray-100 flex items-center justify-center h-screen border:">
        <div className="w-full max-w-xs">
          <div className="bg-white border border-black shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <div className="flex mb-4">
              <button
                className={`flex-1 py-1 px-2 border border-black rounded-l-full text-center font-semibold ${
                  activeTab === 'user' ? 'bg-[#6FCBFF] text-white' : 'bg-gray-200 text-gray-700'
                }`}
                onClick={() => setActiveTab('user')}
              >
                User
              </button>
              <button
                className={`flex-1 py-1 px-2 border border-black rounded-r-full text-center font-semibold ${
                  activeTab === 'guest' ? 'bg-[#6FCBFF] text-white' : 'bg-gray-200 text-gray-700'
                }`}
                onClick={() => setActiveTab('guest')}
              >
                Guest
              </button>
            </div>
            <div>
              {activeTab === 'user' ? (
                <form onSubmit={handleLogin}>
                  <div className="mb-4">
                    <label className="block text-gray-700 text-sm mb-2" htmlFor="email">
                      Email
                    </label>
                    <input
                      className="shadow appearance-none border rounded-lg border border-black w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      id="email"
                      type="email"
                      name="email"
                      placeholder="Email"
                    />
                  </div>
                  <div className="mb-6">
                    <label className="block text-gray-700 text-sm mb-2" htmlFor="password">
                      Password
                    </label>
                    <input
                      className="shadow appearance-none border rounded-lg border border-black w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                      id="password"
                      type="password"
                      name="password"
                      placeholder="******************"
                    />
                  </div>
                  <div className="flex items-center justify-center">
                    <button
                      className="bg-green-500 hover:bg-green-700 border border-black text-white py-2 px-4 rounded-lg focus:outline-none focus:shadow-outline"
                      type="submit"
                    >
                      <p className="">Login</p>
                    </button>
                  </div>
                </form>
              ) : (
                <form onSubmit={handleLogin}>
                <div className="flex items-center justify-center">
                    <button
                      className="my-20 bg-green-500 hover:bg-green-700 border border-black text-white py-2 px-4 rounded-lg focus:outline-none focus:shadow-outline"
                      type="submit"
                    >
                      Login
                    </button>
                </div>
                </form>
              )
              }
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
