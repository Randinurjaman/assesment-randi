'use client'

import { Generate } from "../be-assesment/controller-api";

export default function PostList() {
  return( 
  <>   
    <div className="bg-gray-100 flex items-center justify-center h-screen border:">
        <div className="w-full max-w-xs">
          <div className="bg-white border border-black shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <div className="flex mb-4">
              <button  className="bg-green-500 hover:bg-green-700 border border-black text-white py-2 px-4 rounded-lg focus:outline-none focus:shadow-outline"
                       onClick={() => {Generate()}}
              >
                Generate User
              </button>              
            </div>
          </div>
        </div>
      </div>
  </>
  );
}
