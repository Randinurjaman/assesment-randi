'use server'

import { PrismaClient, type Task } from "@prisma/client";
import { randomBytes } from "crypto";
import { NextResponse } from "next/server";

  
  export async function POST(req: Request) {
    const prisma = new PrismaClient()
    try {
      const body: Task = await req.json();
      console.log(body)
      if (!body.title || typeof body.title!== "string") {
        return new Response(
          JSON.stringify({
            error: "invalid_request",
            message: "Task is required and must be a string",
          }),
          {
            status: 400,
          }
        );
      }     
      const todo = await prisma.task.create({
        data: {
          title: body.title,
          created_at: new Date(),
          updated_at: new Date(),
          status: false,
        },
      });
  
      return new Response(
        JSON.stringify({
          data: todo,
        }),
        {
          status: 201,
        }
      );
    } catch (error) {
      console.error(error);
      return new Response(
        JSON.stringify({
          error: "internal_server_error",
          message: "An unexpected error occurred",
        }),
        {
          status: 500,
        }
  );
}}