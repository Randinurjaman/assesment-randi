'use server'

import { PrismaClient, type Task } from "@prisma/client";
import { randomBytes } from "crypto";
import { NextResponse } from "next/server";


export async function PUT(request: Request,
  { params }: { params: { id: number } }) {
    const prisma = new PrismaClient()
  try {

    const body: Task = await request.json();
    
  const numericId = Number(params.id);
   console.log(numericId)
    const todo = await prisma.task.update({
      data: {
        title: body.title,
        // created_at: new Date(),
        updated_at: new Date(),
        status: false,
        id: numericId
      },
      where: { id: numericId },
    });

    return new Response(
      JSON.stringify({
        data: todo,
      }),
      {
        status: 201,
      }
    );
  } catch (error) {
    console.error(error);
    return new Response(
      JSON.stringify({
        error: "internal_server_error",
        message: "An unexpected error occurred",
      }),
      {
        status: 500,
      }
);
}
}