import { PrismaClient, type Task } from "@prisma/client";
import { randomBytes } from "crypto";
import { NextResponse } from "next/server";

  
  export async function GET(req: Request) {
    const prisma = new PrismaClient()
    const getList = await prisma.task.findMany({
      orderBy: {
        created_at: 'desc',
      },
    });
  
    return new NextResponse(JSON.stringify(getList));
}