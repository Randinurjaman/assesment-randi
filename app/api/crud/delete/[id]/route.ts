'use server'

import { PrismaClient, type Task } from "@prisma/client";
import { randomBytes } from "crypto";
import { NextResponse } from "next/server";


export async function DELETE(request: Request,
  { params }: { params: { id: number } }) {
    const prisma = new PrismaClient()
  try {    
    const numericId = Number(params.id);
    const todo = await prisma.task.delete({     
      where: { id: numericId },
    });

    return new Response(
      JSON.stringify({
        data: todo,
      }),
      {
        status: 201,
      }
    );
  } catch (error) {
    console.error(error);
    return new Response(
      JSON.stringify({
        error: "internal_server_error",
        message: "An unexpected error occurred",
      }),
      {
        status: 500,
      }
    );
    }
}