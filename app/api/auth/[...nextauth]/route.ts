import { PrismaClient } from "@prisma/client"
import { get } from "http"
import NextAuth, { NextAuthOptions } from "next-auth"
import CredentialsProviders from "next-auth/providers/credentials"
import { login } from "../login/route"

const prisma = new PrismaClient()
export const authOptions:NextAuthOptions = {
  // Configure one or more authentication providers
  secret: process.env.NEXTAUTH_SECRET,
    providers: [
        CredentialsProviders({
          name: "Credentials",
          credentials: {
            email: {
              label: "Email",
              type: "email",
              placeholder: "Insert Email Here...",
            },
            password: { label: "Password", type: "password" },
          },
          async authorize(credentials, req) {
            var { email, password, type_login } = credentials as any;
            var user: any;
            user = await login(email, password);
            let userInfo = user.resp_data;
            if (type_login === "guest") {
              userInfo = {
                name: "Guest",
                email: "",
                picture: "",
              };
            } else if (type_login === "login") {
              userInfo = {
                ...userInfo,
                name: userInfo.fullname,
                email: userInfo.email,
                picture: "",
              };
            } else {
              if (user.resp_code === "02") {
                return null;
              }
            }
            return { ...userInfo };
          },
        }),
      ],
    callbacks: {
    async jwt({ token, account }) {
        // Persist the OAuth access_token to the token right after signin
        if (account) {
        token.accessToken = account.access_token
        }
        return token
    },
    async signIn(signInc) {
        return true;
      },
      async redirect({ url, baseUrl }) {
        return baseUrl;
      },
    async session({ session, token, user }) {
        // Send properties to the client, like an access_token from a provider.
        // session.accessToken = token.accessToken
        return session
    },
    },
    pages: { signIn: "/" },
    events: {
        async signOut({ session, token }) {
        // await logout((token as any).refresh_token);
        },
    },
    session: {
        strategy: "jwt",
    },
}
const handler = NextAuth(authOptions)
export {handler as GET, handler as POST}