'use server'
import { PrismaClient } from "@prisma/client";
import { NextResponse } from "next/server";
import * as bcrypt from 'bcrypt';
import type { Task } from "@prisma/client";


export async function Generate() {
  const prisma = new PrismaClient()
  const passBycrpt = '12345678';
  const hassPass = await bcrypt.hash(passBycrpt, 10)
  const user = await prisma.user.createMany({
    data: [
      {
        name: 'Assesment',
        email: 'assesment@email.com',
        password: String(hassPass),
      },
      {
        name: 'Guest',
        email: 'guest@email.com',
        password: String(hassPass),
      },    
    ]
  })
  console.log(user)
}