"use client";

import { useSession } from "next-auth/react";
import { useEffect, useState } from "react";
import { Circle, CircleAlert, CircleCheck, CircleDashed, CircleX, LucideCircle, LucideCircleX, Pencil } from 'lucide-react';
import { PrismaClient, type Task } from "@prisma/client";
const prisma = new PrismaClient();
import moment from 'moment';

function TaskManagement() {
  const [activeTab, setActiveTab] = useState<'task' | 'profile'>('task');
  const session = useSession()
  console.log(session)

  const [tasks, setTasks] = useState<Task[]>([]);
  const [title, setTitle] = useState("");
  const [idData, setIdData] = useState(Number);
  const [show, setShow] = useState(false);
  

  const handleTitleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(event.target.value);
  };
  const update = async (id: number, title: string) => {
    setTitle(title);
    setIdData(id);
    setShow(true);
    console.log(id, title)
  }
  const notShow = async () => {
    setShow(false);
    setTitle("");
  }
  
  const GetList = async () => {
    const res = await fetch("http://localhost:3000/api/crud", {
      cache: "no-store",
      headers: {
       "Content-Type": "application/json",
      },
    });
    const resp = await res.json();
    console.log("🚀 ~ GetList ~ resp:", resp)
    setTasks(resp);
  };
  const addTask = async () => {
    if (title.trim() !== "") {
      const newTask: Task = {
        title: title,
        created_at: new Date(),
        updated_at: new Date(),
        status: false,
        id: Math.floor(Math.random() * 1000000),
        created_by: null,
        updated_by: null
      };      
      await fetch("http://localhost:3000/api/crud/create", {
        method: "POST",
        headers: { "Content-Type": "application/json",
      },
        body: JSON.stringify(newTask),
      });
      GetList();
      setTasks([...tasks]);
      setTitle("");
    }
  };
  const deleteTask = async (id: number) => {     
    await fetch(`http://localhost:3000/api/crud/delete/${id}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json",
    },
    });
    GetList();
    setTasks([...tasks]);
  };
  const UpdateTask = async () => {
    if (title.trim() !== "") {
      const newTask: Task = {
        title: title,
        created_at: new Date(),
        updated_at: new Date(),
        status: false,
        id: idData,
        created_by: null,
        updated_by: null
      };      
      await fetch(`http://localhost:3000/api/crud/update/${idData}`, {
        method: "PUT",
        headers: { "Content-Type": "application/json",
      },
        body: JSON.stringify(newTask),
      });
      GetList();
      setTasks([...tasks]);
      setTitle("");
    }
  };
  useEffect(() => {
    GetList();
  }, [])
  return (
    <>
     <nav className="bg-white shadow-lg">
      <div className="max-w-6xl mx-auto px-4">
        <div className="flex justify-between">
          <div className="flex space-x-7">           
            {/* Primary Navbar items */}
            <div className="hidden md:flex items-center space-x-1">
              <button 
              className="py-4 px-2 text-black-500 font-semibold hover:text-blue-500 transition duration-300"
              onClick={() => setActiveTab('task')} 
              >
                Task
              </button>
              {session.data?.user?.name !== 'Guest' ?(
                 <button 
                 className="py-4 px-2 text-black-500 font-semibold hover:text-blue-500 transition duration-300"
                 onClick={() => setActiveTab('profile')} 
                 >
                   Profile
                 </button>
              ) : (
                <></>
              )}             
            </div>
          </div>
          {/* Secondary Navbar items */}
          <div className="hidden md:flex items-center space-x-3 ">
            <p className="py-2 px-2 font-medium text-black-500 rounded hover:bg-black-200 transition duration-300">{session.data?.user?.name}</p>
          </div>
          {/* Mobile menu button */}
          <div className="md:hidden flex items-center">
            <button className="outline-none mobile-menu-button">
              <svg className=" w-6 h-6 text-gray-500 hover:text-blue-500 " x-show="!showMenu" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" stroke="currentColor">
                <path d="M4 6h16M4 12h16M4 18h16"></path>
              </svg>
            </button>
          </div>
        </div>
      </div>     
      <hr className="border border-black"/>
    </nav>
    {activeTab === 'task' ? (
    <main className="flex min-h-screen flex-col items-center justify-between p-20 bg-white">
      <div className="rounded-lg items-center justify-center p-4 w-1/3">
        <div className="container mx-auto p-4">
          <h1 className="flex justify-center text-4xl mb-4 text-black">
            Task Management
          </h1>
          <div className="mb-4">
            <label
              htmlFor="title"
              className="block text-gray-700 font-bold mb-2"
            >
              Title
            </label>
            <input
              type="text"
              id="title"
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              value={title}
              onChange={handleTitleChange}
            />
          </div>
          <div className="flex justify-center">
            {show !== true ? 
            <>
            <button              
                className="bg-[#6FCBFF] hover:bg-blue-500 text-black py-1 px-2 text-sm mr-2 rounded-lg focus:outline-none focus-shadow-outline"
                onClick={addTask}
              >
                Add Task
            </button>
            </>
            :
            <>
            <button              
              className="bg-[#FFB46F] hover:bg-orange-500 text-black py-1 px-2 text-sm mr-2 rounded-lg focus:outline-none focus-shadow-outline"
              onClick={UpdateTask}
            >
              Update Task
            </button>            
            <button              
              className="bg-[#FF6F6F] hover:bg-red-500 text-black py-1 px-2 text-sm mr-2 rounded-lg focus:outline-none focus-shadow-outline"
              onClick={notShow}
            >
              Cancel
            </button>            
            </>
            }
           

          </div>
          <div className="container mx-auto">
            <h2 className="text-2xl font-bold mb-4 text-black">Ongoing Task</h2>
            <ul className="mt-4">
              {tasks.map((task, index) => {
                if (!task.status) {
                  return (
                    <li
                      key={index}
                      className="mb-2 flex items-center justify-between bg-gray-300 p-3 rounded-md"
                    >
                      <div className="items-center">
                        <div className="inline-flex">
                          <span className="text-black-700 text-ellipsis overflow-hidden line-clamp-2">{task.title}</span> 
                          <button onClick={() => update(task.id, String(task.title))}>
                            <Pencil style={{ marginLeft: '8px' }} color="black" size={15} />                            
                          </button>
                        </div>
                        <span className="text-black-500 text-sm flex items-center justify-content-start">
                          {String(moment(task.created_at).format('DD MMM yyyy HH:mm'))}
                        </span>
                      </div>
                      <div className="inline-flex">
                        <button              
                          onClick={()=>{
                            deleteTask(task.id)
                          }}
                        >
                           <LucideCircleX style={{ marginLeft: '8px' }} color="black" size={20} /> 
                        </button>
                        <button
                          className="px-2 py-1 rounded bg-white-300 text-white-700 hover:bg-white-300"
                          onClick={() => {
                            setTasks(
                              tasks.map((t, i) =>
                                i === index
                                  ? { ...t, status: !t.status }
                                  : t
                              )
                            );
                          }}
                        >
                         <LucideCircle style={{ marginLeft: '8px', background: 'white'}} color="black" size={20} />
                        </button>
                        
                      </div>
                    </li>
                  );
                }
              })}
            </ul>

            <h2 className="text-2xl font-bold mb-4 mt-8 text-black">
              Completed Task
            </h2>
            <ul className="mt-4">
              {tasks.map((task, index) => {
                if (task.status) {
                  return (
                    <li
                      key={index}
                      className="mb-2 flex items-center justify-between bg-gray-300 p-3 rounded-md"
                    >
                      <div className="items-center">
                        <div className="inline-flex">
                          <span className="text-black-700 line-through text-ellipsis overflow-hidden line-clamp-2">{task.title}</span>
                            <Pencil style={{ marginLeft: '8px' }} color="black" size={15} />
                        </div>
                          <span className="text-black-500 text-sm flex items-center justify-content-start">
                          {String(moment(task.created_at).format('DD MMM yyyy HH:mm'))}
                          </span>
                      </div>
                      <div className="inline-flex">
                        <button              
                          onClick={()=>{
                            deleteTask(task.id)
                          }}
                        >
                           <LucideCircleX style={{ marginLeft: '8px' }} color="black" size={20} /> 
                        </button>
                        <button
                          className="px-2 py-1 rounded bg-gray-300 text-gray-700 hover:bg-gray-300"
                          onClick={() => {
                            setTasks(
                              tasks.map((t, i) =>
                                i === index
                                  ? { ...t, status: !t.status }
                                  : t
                              )
                            );
                          }}
                        >
                         <CircleCheck style={{ marginLeft: '8px' }} color="black" size={20} />
                        </button>
                      </div>
                    </li>
                  );
                }
              })}
            </ul>
          </div>
        </div>
      </div>
    </main>
     ) : (
      <main className="flex min-h-screen flex-col items-center justify-between p-20 bg-white">
      <div className="rounded-lg items-center justify-center p-4 w-1/3">
        <div className="container mx-auto p-4 mt-20">
          <p>email: {session.data?.user?.email}</p>
          <p>fullname: {session.data?.user?.name}</p>
        </div>
      </div>
    </main>
    )}
    </>
  );
}

export default TaskManagement;
